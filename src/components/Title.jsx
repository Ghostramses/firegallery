import React from 'react';

const Title = () => {
	return (
		<div className='title'>
			<h1>Galeria de Fotos</h1>
			<h2>FireGallery</h2>
			<p>Sube tus fotos aqui!</p>
		</div>
	);
};

export default Title;
