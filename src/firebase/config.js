import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';

// Your web app's Firebase configuration
var firebaseConfig = {
	apiKey: 'AIzaSyAPqpTwqIj2CEJwcSQQDif9wku3Krreca4',
	authDomain: 'firegalery-78e72.firebaseapp.com',
	projectId: 'firegalery-78e72',
	storageBucket: 'firegalery-78e72.appspot.com',
	messagingSenderId: '959751416747',
	appId: '1:959751416747:web:3bc02469bc31f1e02b40ee'
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const projectStorage = firebase.storage();
const projectFirestore = firebase.firestore();
const timestamp = firebase.firestore.FieldValue.serverTimestamp;

export { projectStorage, projectFirestore, timestamp };
